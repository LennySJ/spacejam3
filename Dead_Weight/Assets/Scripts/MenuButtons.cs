using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public GameObject Crossfade;
    private void Start()
    {
        Crossfade.SetActive(false);
    }
    public void Play()
    {
        StartCoroutine(MenuButton(true));
        Crossfade.SetActive(true);
    }

    public void Quit()
    {
        StartCoroutine(MenuButton(false));
    }

    IEnumerator MenuButton(bool play)
    {
        yield return new WaitForSecondsRealtime(1f);
        if (play)
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            Application.Quit();
        }
    }
}
