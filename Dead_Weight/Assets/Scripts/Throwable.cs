﻿using System.Collections;
using UnityEngine;

public class Throwable : MonoBehaviour
{
    [SerializeField] private AudioClip throwingthud;

    public bool isThrowable;

    public float throwingForce;
    public Collider2D col;
    private Rigidbody2D rb2d;

    private KillerChain killerChain;
    private KillerChainEnemy killerChainEnemy;

    public Animator anim;

    public Collider2D killCollider;

    CorpseStates _corpseState;
    CorpseStates CorpseState
    {
        get { return _corpseState; }
        set
        {
            _corpseState = value;
            UpdateCorpseState();
        }
    }

    private enum CorpseStates
    {
        Lying,
        Flying,
        PickedUp
    }

    protected virtual void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        killerChain = GetComponentInChildren<KillerChain>();
        killerChainEnemy = GetComponentInChildren<KillerChainEnemy>();
    }

    private void Update()
    {
        if (!isThrowable) return;
        switch (CorpseState)
        {
            case CorpseStates.Lying:
                break;
            case CorpseStates.Flying:
                break;
            case CorpseStates.PickedUp:
                transform.position = Player.Instance.carryPosition.position;
                Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
                difference.Normalize();
                float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(0f, 0f, rotationZ - 90);
                if (Input.GetMouseButtonDown(0))
                {
                    Throw();
                }
                break;
            default:
                break;
        }
    }

    public void Throw()
    {
        Player.Instance.SetMoveSpeed(1f);
        Player.Instance.SetDashable(false);
        //Player.Instance.SetIsCarrying(false);
        CorpseState = CorpseStates.Flying;
        Vector3 dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        dir.z = 0f;
        dir.Normalize();
        rb2d.AddForce(dir.normalized * throwingForce, ForceMode2D.Impulse);
        if (killerChain != null)
        {
            killerChain.canKill = true;
        }
        Debug.Log("Throw");
        StartCoroutine(Carry());
        StartCoroutine(PlayThrowAnim());
        //if (killerChainEnemy != null) killerChainEnemy.canKill = true;
    }

    IEnumerator PlayThrowAnim()
    {
        if (transform.gameObject.name == "Corpse")
        {
            Player.Instance.GetComponent<PlayerAnimControl>().PlayThrow();
            yield return new WaitForSeconds(0.1f);
            Player.Instance.GetComponent<AudioSource>().clip = throwingthud;
            Player.Instance.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.4f);
            Player.Instance.GetComponent<PlayerAnimControl>().PlayIdle();
        }
        else if (GetComponent<Enemy>() != null)
        {
            GetComponent<Animator>().SetInteger("AnimParameter", 4);
            yield return new WaitForSeconds(0.1f);
            Debug.Log("PlayEnemyThud");
            Player.Instance.GetComponent<AudioSource>().clip = throwingthud;
            Player.Instance.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.4f);
            GetComponent<Animator>().SetInteger("AnimParameter", 3);
            Debug.Log("ThrowObject");
        }
        else if (GetComponent<Obstacle>() != null)
        {
            Debug.Log("Obstacle???");
            GetComponent<Obstacle>().isThrowable = false;
            GetComponent<Obstacle>().GetComponent<Collider2D>().isTrigger = false;
            GetComponent<Rigidbody2D>().collisionDetectionMode = CollisionDetectionMode2D.Continuous;
            yield return new WaitForSeconds(0.1f);
            Player.Instance.GetComponent<AudioSource>().clip = throwingthud;
            Player.Instance.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.15f);
            GetComponent<Obstacle>().isThrowable = true;
            GetComponent<Obstacle>().GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Rigidbody2D>().collisionDetectionMode = CollisionDetectionMode2D.Discrete;
        }
    }

    private void Lying()
    {
        if (transform.gameObject.name == "Corpse")
        {
            anim.SetInteger("CorpseAnimParameter", 0);
        }
        if (GetComponent<Enemy>() != null)
        {
            anim.SetInteger("AnimParameter", 3);
        }
    }

    private void PickedUp()
    {
        Player.Instance.SetDashable(true);
        Player.Instance.SetIsCarrying(true);
        if (killerChain != null)
        {
            killerChain.canKill = false;
        }
        if (killerChainEnemy != null)
        {
            killerChainEnemy.canKill = false;
            //Activate Collider that is in KillerChain Object (Can be killerChain.collider.enabled = true;)
            killCollider.enabled = true;
        }
        if (transform.gameObject.name == "Corpse")
        {
            anim.SetInteger("CorpseAnimParameter", 1);
        }
        if (GetComponent<Enemy>() != null)
        {
            anim.SetInteger("AnimParameter", 2);
        }
    }

    private void Flying()
    {
        StartCoroutine(PlayFlying());
    }

    IEnumerator PlayFlying()
    {
        if (transform.gameObject.name == "Corpse")
        {
            anim.SetInteger("CorpseAnimParameter", 2);
        }
        if (GetComponent<Enemy>() != null)
        {
            anim.SetInteger("AnimParameter", 4);
        }
        yield return new WaitForSeconds(0.35f);
        if (transform.gameObject.name == "Corpse")
        {
            anim.SetInteger("CorpseAnimParameter", 0);
        }
        if (GetComponent<Enemy>() != null)
        {
            anim.SetInteger("AnimParameter", 3);
        }
    }

    private void UpdateCorpseState()
    {
        switch (CorpseState)
        {
            case CorpseStates.Lying:
                col.enabled = true;
                Lying();
                break;
            case CorpseStates.Flying:
                col.enabled = true;
                Flying();
                break;
            case CorpseStates.PickedUp:
                if (GetComponent<Obstacle>() == null)
                {
                    col.enabled = false;
                }
                PickedUp();
                break;
            default:
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player" && !Player.Instance.isCarrying)
        {
            // pick up
            Debug.Log(Player.Instance.isCarrying);
            CorpseState = CorpseStates.PickedUp;
        }
    }

    //private void OnTriggerExit2D(Collider2D Collision)
    //{
    //    if (Collision.name == "Player")
    //    {
    //        Debug.Log("Exit");
    //        if (killerChainEnemy != null) killerChainEnemy.canKill = true;
    //        StartCoroutine(Carry());
    //    }
    //}

    private IEnumerator Carry()
    {
        yield return new WaitForSeconds(0.1f);
        Player.Instance.SetIsCarrying(false);
        if (killerChainEnemy != null)
        {
            killerChainEnemy.canKill = true;
            //Activate Collider that is in KillerChain Object (Can be killerChain.collider.enabled = false;)
            killCollider.enabled = false;
        }
    }
}
