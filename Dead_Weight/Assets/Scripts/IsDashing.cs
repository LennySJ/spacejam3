using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsDashing : MonoBehaviour
{
    public static bool isDashing;
    //call these at the beginning and end of the dash animation
    public void Dash()
    {
        isDashing = true;
    }
    public void EndDash()
    {
        isDashing = false;
        transform.Rotate(0, 0, 0);
    }
}
