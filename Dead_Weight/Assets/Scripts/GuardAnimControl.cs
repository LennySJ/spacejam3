using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardAnimControl : MonoBehaviour
{
    public Animator guardAnimator;

    public void PlayDead() {
        guardAnimator.SetInteger("AnimParameter", 3);
    }

    public void PlayCarried() {
        guardAnimator.SetInteger("AnimParameter", 2);
    }

    public void PlayRun() {
        guardAnimator.SetInteger("AnimParameter", 1);
    }

    public void PlayThrow() {
        guardAnimator.SetInteger("AnimParameter", 4);
    }

    public void PlayIdle() {
        guardAnimator.SetInteger("AnimParameter", 0);
    }
}
