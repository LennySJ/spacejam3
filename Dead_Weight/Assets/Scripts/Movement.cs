﻿using System.Collections;
using UnityEngine;

public class Movement : MonoBehaviour
{
    float verti;
    float horizont;
    public float speed;
    public Rigidbody2D rb;
    public Vector2 direction;
    public bool dash;
    public float dashSpeed;
    public float dashTime;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (dash)
        {
            rb.AddForce(direction * dashSpeed);
        }
        else
        {
            horizont = Input.GetAxisRaw("Horizontal");
            verti = Input.GetAxisRaw("Vertical");
            direction = new Vector2(horizont, verti);
        }

        if (Input.GetButtonDown("Fire3") && !IsDashing.isDashing)
        {
            //here start the roll animation, I'd use bools
            StartCoroutine(Dash());
        }
    }

    private void FixedUpdate()
    {
        if (Mathf.Abs(direction.magnitude) > 0.001)
        {
            if (GetComponent<Player>().isCarrying == true)
            {
                GetComponent<PlayerAnimControl>().PlayWalk();
                GetComponent<Footsteps>().WalkingSound();
            }
            else
            {
                GetComponent<PlayerAnimControl>().PlayRun();
                GetComponent<Footsteps>().RunningSound();
            }
        }
        else
        {
            GetComponent<PlayerAnimControl>().PlayIdle();
            GetComponent<Footsteps>().NoSound();
        }
        rb.velocity = direction * speed;
    }

    IEnumerator Dash()
    {
        dash = true;
        GetComponent<PlayerAnimControl>().PlayDash();
        yield return new WaitForSeconds(dashTime);
        dash = false;
        GetComponent<PlayerAnimControl>().PlayIdle();
    }
}
