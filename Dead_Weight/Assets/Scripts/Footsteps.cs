﻿using System.Collections;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    [SerializeField] private AudioClip footsteps;
    [SerializeField] private GameObject footStepSound;
    private bool moving;
    private bool running;

    public void WalkingSound()
    {
        if (!moving || running)
        {
            moving = true;
            running = false;
            StopAllCoroutines();
            footStepSound.GetComponent<AudioSource>().Stop();
        }
        if (footStepSound.GetComponent<AudioSource>().isPlaying == false)
        {
            StartCoroutine(SlowFootsteps());
        }
    }

    IEnumerator SlowFootsteps()
    {
        footStepSound.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.6f);
        StartCoroutine(SlowFootsteps());
    }

    public void RunningSound()
    {
        if (!running)
        {
            running = true;
            StopAllCoroutines();
            footStepSound.GetComponent<AudioSource>().Stop();
        }
        if (footStepSound.GetComponent<AudioSource>().isPlaying == false)
        {
            StartCoroutine(FastFootsteps());
        }
    }

    IEnumerator FastFootsteps()
    {
        footStepSound.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.3f);
        StartCoroutine(FastFootsteps());
    }

    public void NoSound()
    {
        if (moving || running)
        {
            StopAllCoroutines();
            footStepSound.GetComponent<AudioSource>().Stop();
        }
    }
}
