using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corpse : Throwable
{
    protected override void Start()
    {
        base.Start();
        isThrowable = true;
    }
}
