using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimControl : MonoBehaviour
{
    public Animator playerAnimator;

    public void PlayDash() {
        playerAnimator.SetInteger("AnimParameter", 3);
    }

    public void PlayWalk() {
        playerAnimator.SetInteger("AnimParameter", 2);
    }

    public void PlayRun() {
        playerAnimator.SetInteger("AnimParameter", 1);
    }

    public void PlayThrow() {
        playerAnimator.SetInteger("AnimParameter", 4);
    }

    public void PlayIdle() {
        playerAnimator.SetInteger("AnimParameter", 0);
    }
}
