using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerChain : MonoBehaviour
{
    [SerializeField] private Transform corpse;
    [SerializeField] private LayerMask layerMask;

    public Color closeColor;
    public Color farColor;
    public bool canKill;
    private SpriteRenderer sprite;

    private void Start()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        canKill = true;
    }

    private void Update()
    {
        // every frame check for a layer between player and corpse
        if (canKill)
            RayCast();
        FadeColor();
    }

    private void LateUpdate()
    {
        LookAt();
        Scale();
    }

    private void FadeColor()
    {
        sprite.color = Color.Lerp(closeColor, farColor, Vector2.Distance(transform.position, corpse.position));
    }

    private void Scale()
    {
        transform.localScale = new Vector3(Vector2.Distance(corpse.position, transform.position) / 0.15f, 1, 1);
    }

    private void LookAt()
    {
        Vector3 difference = corpse.position - transform.position;
        difference.Normalize();
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotationZ);
    }

    private void RayCast()
    {
        Vector3 anchorPosition = corpse.position;
        Vector3 startPosition = transform.position;
        anchorPosition.z += 2000f;
        startPosition.z += 2000f;

        RaycastHit2D hit = Physics2D.Raycast(startPosition, anchorPosition - startPosition, Vector2.Distance(anchorPosition, startPosition), layerMask);
        if (hit.collider == null) return;
        if (hit.collider.CompareTag("Destructible"))
        {
            hit.collider.gameObject.GetComponent<IKillable>().Die();

#if UNITY_EDITOR
            Debug.DrawRay(startPosition, anchorPosition - startPosition, Color.green);
#endif
        }
        else
        {
#if UNITY_EDITOR
            Debug.DrawRay(startPosition, anchorPosition - startPosition, Color.red);
#endif
        }


        anchorPosition.z -= 2000f;
        startPosition.z -= 2000f;
        hit = Physics2D.Raycast(anchorPosition, startPosition - anchorPosition, Vector2.Distance(anchorPosition, startPosition), layerMask);
        if(hit.collider == null) return;
        if(hit.collider.CompareTag("Destructible")) {
            hit.collider.gameObject.GetComponent<IKillable>().Die();

#if UNITY_EDITOR
            Debug.DrawRay(anchorPosition, startPosition - anchorPosition, Color.green);
#endif
        }
        else {
#if UNITY_EDITOR
            Debug.DrawRay(anchorPosition, startPosition - anchorPosition, Color.red);
#endif
        }
    }
}
