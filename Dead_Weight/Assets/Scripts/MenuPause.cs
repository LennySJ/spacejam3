using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPause : MonoBehaviour
{
    bool MenuOpen;
    public GameObject PauseCanvas;
    private void Start()
    {
        PauseCanvas.SetActive(false);
        MenuOpen = false;
        Time.timeScale = 1;
    }
    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (MenuOpen)
            {
                PauseCanvas.SetActive(false);
                MenuOpen = false;
                Time.timeScale = 1;
            }
            else
            {
                PauseCanvas.SetActive(true);
                MenuOpen = true;
                Time.timeScale = 0;
            }
        }
    }

    public void Resume()
    {
        PauseCanvas.SetActive(false);
        MenuOpen = false;
        Time.timeScale = 1;
    }

    public void QuitToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
