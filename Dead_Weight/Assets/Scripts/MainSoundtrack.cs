﻿using UnityEngine;

public class MainSoundtrack : MonoBehaviour
{
    [SerializeField] private AudioClip mainmusic;

    private void Start()
    {
        GetComponent<AudioSource>().clip = mainmusic;
        GetComponent<AudioSource>().loop = true;
        GetComponent<AudioSource>().Play();
    }
}
