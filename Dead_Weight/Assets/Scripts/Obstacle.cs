﻿using UnityEngine;
using UnityEngine.AI;

public class Obstacle : Throwable, IKillable
{
    [SerializeField] private AudioClip CuttinLaser;
    private bool once;
    private bool dead;
    protected override void Start()
    {
        base.Start();
        dead = false;
    }

    public void Die()
    {
        if(dead != true) {
            dead = true;
            isThrowable = true;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<NavMeshObstacle>().enabled = true;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            if(!once) {
                GetComponent<AudioSource>().clip = CuttinLaser;
                GetComponentInChildren<ParticleSystem>().Play();
                GetComponent<AudioSource>().Play();
                once = true;
            }
        }
    }
}
