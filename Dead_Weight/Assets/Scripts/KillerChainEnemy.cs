using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerChainEnemy : MonoBehaviour
{
    [SerializeField] private Transform anchor;
    [SerializeField] private LayerMask layerMask;

    public Color closeColor;
    public Color farColor;
    public bool canKill;
    private SpriteRenderer sprite;
    private string instanceName;

    public float value;

    private void Start()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        canKill = true;
        instanceName = transform.parent.parent.name;
    }

    private void Update()
    {
        // every frame check for a layer between player and corpse
        if (canKill)
            RayCast();
        FadeColor();
    }

    private void LateUpdate()
    {
        LookAt();
        Scale();
    }

    private void FadeColor()
    {
        sprite.color = Color.Lerp(closeColor, farColor, Vector2.Distance(transform.position, anchor.position));
    }

    private void Scale()
    {
        transform.localScale = new Vector3(Vector2.Distance(anchor.position, transform.position) / value, 1, 1);
    }

    private void LookAt()
    {
        Vector3 difference = anchor.position - transform.position;
        difference.Normalize();
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotationZ);
    }

    private void RayCast()
    {
        Vector3 anchorPosition = anchor.position;
        anchorPosition.z += 200f;
        Vector3 startPosition = transform.position;
        startPosition.z += 200f;

        RaycastHit2D[] hits = Physics2D.RaycastAll(anchorPosition, startPosition - anchorPosition, Vector2.Distance(anchorPosition, startPosition), layerMask);
        for (int i = 0; i < hits.Length; i++)
        {
            //if (hit.collider == null) return;
            if (hits[i].collider.CompareTag("Destructible") && hits[i].collider.name != instanceName)
            {
                hits[i].collider.gameObject.GetComponent<IKillable>().Die();
            }
        }
    }
}
