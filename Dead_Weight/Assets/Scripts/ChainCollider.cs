using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainCollider : MonoBehaviour
{
	private void OnTriggerEnter2D(Collider2D collision) {
		if(collision.gameObject.name != "Player" && collision.gameObject.tag == "Destructible") {
			collision.gameObject.GetComponent<IKillable>().Die();
		}
	}
}
