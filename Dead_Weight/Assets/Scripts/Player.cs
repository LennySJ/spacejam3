using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour, IKillable
{
    private static Player _instance;
    public static Player Instance { get { return _instance; } private set { _instance = value; } }

    private Movement movement;
    public Transform carryPosition;

    public bool isCarrying;

    public float carrySpeed = 3f;
    public float walkSpeed = 5f;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        movement = GetComponent<Movement>();
        SetMoveSpeed(walkSpeed);
    }

    public void SetMoveSpeed(float value)
    {
        movement.speed = value;
    }

    public void SetDashable(bool state)
    {
        IsDashing.isDashing = state;
    }

    public void SetIsCarrying(bool state)
    {
        isCarrying = state;
        if (state) SetMoveSpeed(carrySpeed);
        else SetMoveSpeed(walkSpeed);
    }

    public void Die()
    {
        Debug.Log("Player Die");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
