﻿using UnityEngine;
using UnityEngine.AI;

public class Enemy : Throwable, IKillable
{
    [SerializeField] private AudioClip CuttinLaser;
    private AgentScript agent;
    private bool once;
    protected override void Start()
    {
        base.Start();
        agent = GetComponent<AgentScript>();
        isThrowable = false;
        GetComponent<NavMeshObstacle>().enabled = false;
    }

    public void Die()
    {
        if (agent.agent.isActiveAndEnabled)
        {
            agent.agent.SetDestination(transform.position);
            agent.agent.enabled = false;
            agent.enabled = false;
            isThrowable = true;
            GetComponent<CircleCollider2D>().isTrigger = true;
            GetComponent<NavMeshObstacle>().enabled = true;
            anim.SetInteger("AnimParameter", 3);
            if (!once)
            {
                GetComponent<AudioSource>().clip = CuttinLaser;
                GetComponentInChildren<ParticleSystem>().Play();
                GetComponent<AudioSource>().Play();
                once = true;
            }
        }
    }
}
