using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AgentScript : MonoBehaviour
{
    public Transform target;

    [HideInInspector] public NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
    }

    void Update()
    {
        agent.SetDestination(target.position);
        if(GetComponent<Animator>() != null) {
            GetComponent<Animator>().SetInteger("AnimParameter", 1);
		}
    }
}
