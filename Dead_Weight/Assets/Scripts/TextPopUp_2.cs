using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPopUp_2 : MonoBehaviour
{
	public GameObject textGameObject;
	public string objectType;
	public static bool keyObtained;

	private void OnTriggerEnter2D(Collider2D collision) {
		if(collision.gameObject.name == "Player") {
			StartCoroutine(PopUpText());
		}
	}

	IEnumerator PopUpText() {
		if(objectType == "Key") {
			textGameObject.GetComponent<Text>().text = "KeyCard Obtained";
			textGameObject.SetActive(true);
			GetComponent<SpriteRenderer>().enabled = false;
			GetComponent<Collider2D>().enabled = false;
			yield return new WaitForSeconds(2.5f);
			textGameObject.SetActive(false);
			keyObtained = true;
			Destroy(this.gameObject);
		}
		else if(objectType == "Door") {
			if(keyObtained != true) {
				textGameObject.GetComponent<Text>().text = "Door Is Locked";
				textGameObject.SetActive(true);
				yield return new WaitForSeconds(2.5f);
				textGameObject.SetActive(false);
			}
			else {
				textGameObject.GetComponent<Text>().text = "Door Opened";
				textGameObject.SetActive(true);
				transform.parent.GetComponent<SpriteRenderer>().enabled = false;
				transform.parent.GetComponent<Collider2D>().enabled = false;
				yield return new WaitForSeconds(2.5f);
				textGameObject.SetActive(false);
				keyObtained = false;
				Destroy(transform.parent.gameObject);
			}
		}
		
	}
}
